﻿Public Class ProcessStart

    Public Delegate Sub EmptyDelegate()

    Public Shared Sub StartShell(parent As Form, program As String, Optional args As String = "", Optional completed As EmptyDelegate = Nothing)
        Static processesRunning As Integer = 0
        Static frmLoading As frmLoading = Nothing

        Dim p As New Process()
        Dim pi As ProcessStartInfo = p.StartInfo
        pi.CreateNoWindow = True
        pi.WindowStyle = ProcessWindowStyle.Hidden
        pi.UseShellExecute = False
        pi.Arguments = args
        pi.FileName = program

        p.EnableRaisingEvents = True
        AddHandler p.Exited, Sub()
                                 processesRunning -= 1

                                 If processesRunning = 0 Then
                                     Dim del As New EmptyDelegate(Sub()
                                                                      parent.Enabled = True
                                                                      frmLoading.Close()
                                                                      frmLoading.Dispose()
                                                                      frmLoading = Nothing

                                                                      If completed IsNot Nothing Then
                                                                          completed()
                                                                      End If
                                                                  End Sub)
                                     parent.Invoke(del)
                                 End If
                             End Sub
        processesRunning += 1
        If frmLoading Is Nothing Then
            parent.Enabled = False
            frmLoading = New frmLoading
            frmLoading.Show()
            frmLoading.Location = New System.Drawing.Point(parent.Location.X + (parent.Bounds.Width - frmLoading.Width) \ 2, parent.Location.Y + (parent.Bounds.Height - frmLoading.Height) \ 2)
        End If
        p.Start()
    End Sub
End Class
