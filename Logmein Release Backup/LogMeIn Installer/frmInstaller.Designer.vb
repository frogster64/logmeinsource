﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Console
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Console))
        Me.ToolTipButtonInfo = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnUninstall = New System.Windows.Forms.Button()
        Me.btnHelp = New System.Windows.Forms.Button()
        Me.btnWindowsXP = New System.Windows.Forms.Button()
        Me.btnConfig = New System.Windows.Forms.Button()
        Me.btnWindows7 = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnCertificates = New System.Windows.Forms.Button()
        Me.btnVersion = New System.Windows.Forms.Button()
        Me.btnCheckLogMeIn = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblstatus = New System.Windows.Forms.Label()
        Me.pnlBackground = New System.Windows.Forms.Panel()
        Me.pnlBackground.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnUninstall
        '
        Me.btnUninstall.BackColor = System.Drawing.SystemColors.ControlText
        Me.btnUninstall.FlatAppearance.BorderSize = 0
        Me.btnUninstall.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUninstall.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.btnUninstall.ForeColor = System.Drawing.SystemColors.Control
        Me.btnUninstall.Image = Global.LogMeIn_Installer.My.Resources.Resources.Delete_Property_50
        Me.btnUninstall.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUninstall.Location = New System.Drawing.Point(131, 294)
        Me.btnUninstall.Name = "btnUninstall"
        Me.btnUninstall.Size = New System.Drawing.Size(285, 50)
        Me.btnUninstall.TabIndex = 1
        Me.btnUninstall.Text = "Uninstall Logmein"
        Me.btnUninstall.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTipButtonInfo.SetToolTip(Me.btnUninstall, "This will install Mission LogMeIn Software")
        Me.btnUninstall.UseVisualStyleBackColor = False
        '
        'btnHelp
        '
        Me.btnHelp.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.btnHelp.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnHelp.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.btnHelp.Image = Global.LogMeIn_Installer.My.Resources.Resources.Help_48
        Me.btnHelp.Location = New System.Drawing.Point(500, 4)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.Size = New System.Drawing.Size(50, 50)
        Me.btnHelp.TabIndex = 14
        Me.ToolTipButtonInfo.SetToolTip(Me.btnHelp, "This loads a form with info and help about the program")
        Me.btnHelp.UseVisualStyleBackColor = False
        '
        'btnWindowsXP
        '
        Me.btnWindowsXP.BackColor = System.Drawing.SystemColors.ControlText
        Me.btnWindowsXP.FlatAppearance.BorderSize = 0
        Me.btnWindowsXP.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnWindowsXP.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.btnWindowsXP.ForeColor = System.Drawing.SystemColors.Control
        Me.btnWindowsXP.Image = Global.LogMeIn_Installer.My.Resources.Resources.Windows_Logo_481
        Me.btnWindowsXP.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnWindowsXP.Location = New System.Drawing.Point(130, 182)
        Me.btnWindowsXP.Name = "btnWindowsXP"
        Me.btnWindowsXP.Size = New System.Drawing.Size(285, 50)
        Me.btnWindowsXP.TabIndex = 2
        Me.btnWindowsXP.Text = "Windows XP LogMeIn Install"
        Me.btnWindowsXP.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTipButtonInfo.SetToolTip(Me.btnWindowsXP, "This will install an Older version of LogMeIn for older terminals")
        Me.btnWindowsXP.UseVisualStyleBackColor = False
        '
        'btnConfig
        '
        Me.btnConfig.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.btnConfig.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnConfig.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.btnConfig.Image = Global.LogMeIn_Installer.My.Resources.Resources.Vertical_Settings_Mixer
        Me.btnConfig.Location = New System.Drawing.Point(454, 4)
        Me.btnConfig.Name = "btnConfig"
        Me.btnConfig.Size = New System.Drawing.Size(50, 50)
        Me.btnConfig.TabIndex = 13
        Me.ToolTipButtonInfo.SetToolTip(Me.btnConfig, "This shows the version")
        Me.btnConfig.UseVisualStyleBackColor = False
        '
        'btnWindows7
        '
        Me.btnWindows7.BackColor = System.Drawing.SystemColors.ControlText
        Me.btnWindows7.FlatAppearance.BorderSize = 0
        Me.btnWindows7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnWindows7.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.btnWindows7.ForeColor = System.Drawing.SystemColors.Control
        Me.btnWindows7.Image = Global.LogMeIn_Installer.My.Resources.Resources.Windows8_50
        Me.btnWindows7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnWindows7.Location = New System.Drawing.Point(131, 126)
        Me.btnWindows7.Name = "btnWindows7"
        Me.btnWindows7.Size = New System.Drawing.Size(285, 50)
        Me.btnWindows7.TabIndex = 0
        Me.btnWindows7.Text = "Windows 7+ LogMeIn Install"
        Me.btnWindows7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTipButtonInfo.SetToolTip(Me.btnWindows7, "This will install the Comtrex LogMeIn Software")
        Me.btnWindows7.UseVisualStyleBackColor = False
        '
        'btnExit
        '
        Me.btnExit.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExit.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.btnExit.Image = Global.LogMeIn_Installer.My.Resources.Resources.Close_Window_50
        Me.btnExit.Location = New System.Drawing.Point(494, 418)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(56, 57)
        Me.btnExit.TabIndex = 7
        Me.ToolTipButtonInfo.SetToolTip(Me.btnExit, "This will Exit the application")
        Me.btnExit.UseVisualStyleBackColor = False
        '
        'btnCertificates
        '
        Me.btnCertificates.BackColor = System.Drawing.SystemColors.ControlText
        Me.btnCertificates.FlatAppearance.BorderSize = 0
        Me.btnCertificates.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCertificates.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.btnCertificates.ForeColor = System.Drawing.SystemColors.Control
        Me.btnCertificates.Image = Global.LogMeIn_Installer.My.Resources.Resources.Diploma_2_50
        Me.btnCertificates.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCertificates.Location = New System.Drawing.Point(130, 238)
        Me.btnCertificates.Name = "btnCertificates"
        Me.btnCertificates.Size = New System.Drawing.Size(285, 50)
        Me.btnCertificates.TabIndex = 3
        Me.btnCertificates.Text = "Certificates"
        Me.btnCertificates.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTipButtonInfo.SetToolTip(Me.btnCertificates, "This will install Certificates for LogMeIn on Windows XP Embedded Only")
        Me.btnCertificates.UseVisualStyleBackColor = False
        '
        'btnVersion
        '
        Me.btnVersion.BackColor = System.Drawing.SystemColors.ControlText
        Me.btnVersion.FlatAppearance.BorderSize = 0
        Me.btnVersion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnVersion.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.btnVersion.ForeColor = System.Drawing.SystemColors.Control
        Me.btnVersion.Image = Global.LogMeIn_Installer.My.Resources.Resources.Versions50
        Me.btnVersion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVersion.Location = New System.Drawing.Point(130, 70)
        Me.btnVersion.Name = "btnVersion"
        Me.btnVersion.Size = New System.Drawing.Size(285, 50)
        Me.btnVersion.TabIndex = 5
        Me.btnVersion.Text = "Windows Version"
        Me.btnVersion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTipButtonInfo.SetToolTip(Me.btnVersion, "This will show you the windows version with Service Pack information Minimium Req" &
        "uirement is Service Pack 3!")
        Me.btnVersion.UseVisualStyleBackColor = False
        '
        'btnCheckLogMeIn
        '
        Me.btnCheckLogMeIn.BackColor = System.Drawing.SystemColors.ControlText
        Me.btnCheckLogMeIn.FlatAppearance.BorderSize = 0
        Me.btnCheckLogMeIn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCheckLogMeIn.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.btnCheckLogMeIn.ForeColor = System.Drawing.SystemColors.Control
        Me.btnCheckLogMeIn.Image = Global.LogMeIn_Installer.My.Resources.Resources.OpenOn_Browser50
        Me.btnCheckLogMeIn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCheckLogMeIn.Location = New System.Drawing.Point(130, 14)
        Me.btnCheckLogMeIn.Name = "btnCheckLogMeIn"
        Me.btnCheckLogMeIn.Size = New System.Drawing.Size(285, 50)
        Me.btnCheckLogMeIn.TabIndex = 6
        Me.btnCheckLogMeIn.Text = "LogMeIn Check Connection"
        Me.btnCheckLogMeIn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCheckLogMeIn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTipButtonInfo.SetToolTip(Me.btnCheckLogMeIn, "This will load a page to see if you can connect to the LogMeIn Server")
        Me.btnCheckLogMeIn.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.Label1.Location = New System.Drawing.Point(5, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(185, 30)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "LogMeIn Installer :"
        '
        'lblstatus
        '
        Me.lblstatus.AutoSize = True
        Me.lblstatus.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblstatus.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.lblstatus.Location = New System.Drawing.Point(196, 13)
        Me.lblstatus.Name = "lblstatus"
        Me.lblstatus.Size = New System.Drawing.Size(90, 25)
        Me.lblstatus.TabIndex = 15
        Me.lblstatus.Text = "Checking"
        '
        'pnlBackground
        '
        Me.pnlBackground.BackColor = System.Drawing.Color.Black
        Me.pnlBackground.Controls.Add(Me.btnUninstall)
        Me.pnlBackground.Controls.Add(Me.btnCertificates)
        Me.pnlBackground.Controls.Add(Me.btnCheckLogMeIn)
        Me.pnlBackground.Controls.Add(Me.btnWindowsXP)
        Me.pnlBackground.Controls.Add(Me.btnVersion)
        Me.pnlBackground.Controls.Add(Me.btnWindows7)
        Me.pnlBackground.Location = New System.Drawing.Point(-5, 57)
        Me.pnlBackground.Name = "pnlBackground"
        Me.pnlBackground.Size = New System.Drawing.Size(564, 361)
        Me.pnlBackground.TabIndex = 18
        '
        'Console
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ClientSize = New System.Drawing.Size(552, 478)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnHelp)
        Me.Controls.Add(Me.btnConfig)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.lblstatus)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.pnlBackground)
        Me.Font = New System.Drawing.Font("Microsoft YaHei UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Console"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.Text = "LogMeIn Installer"
        Me.ToolTipButtonInfo.SetToolTip(Me, "LogMeIn Installer")
        Me.pnlBackground.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnWindows7 As System.Windows.Forms.Button
    Friend WithEvents btnUninstall As System.Windows.Forms.Button
    Friend WithEvents btnWindowsXP As System.Windows.Forms.Button
    Friend WithEvents btnCertificates As System.Windows.Forms.Button
    Friend WithEvents btnVersion As System.Windows.Forms.Button
    Friend WithEvents btnCheckLogMeIn As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents ToolTipButtonInfo As System.Windows.Forms.ToolTip
    Friend WithEvents btnConfig As System.Windows.Forms.Button
    Friend WithEvents btnHelp As System.Windows.Forms.Button
    Friend WithEvents Label1 As Label
    Friend WithEvents lblstatus As Label
    Friend WithEvents pnlBackground As Panel

End Class
