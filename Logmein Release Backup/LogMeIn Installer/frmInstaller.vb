﻿Imports System.ServiceProcess
Imports System.Diagnostics
Public Class Console
    'Version 4.1.2 LogMeIn Installer

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If (Not System.IO.Directory.Exists("C:\Logmein")) Then
            System.IO.Directory.CreateDirectory("C:\Logmein")
            My.Computer.FileSystem.WriteAllBytes("C:\Logmein\Logmein.msi", My.Resources.LogMeIn, False)
            My.Computer.FileSystem.WriteAllBytes("C:\Logmein\Root Certificate Update.exe", My.Resources.Root_Certificate_Update, False)

            If (Not System.IO.Directory.Exists("C:\Logmein\Legacy")) Then
                System.IO.Directory.CreateDirectory("C:\Logmein\Legacy")
                My.Computer.FileSystem.WriteAllBytes("C:\Logmein\Legacy\Legacy.msi", My.Resources.Legacy, False)
            End If

        End If
        frmSplashScreen.BarLong(100)
        Dim i As Integer = 0
        While i <= 100
            frmSplashScreen.ShowBar(i)
            i += 1
            Threading.Thread.Sleep(100)
        End While
        'BackColor = Color.Azure
        'TransparencyKey = Color.Azure

        'code edit 22/08/2016
    End Sub

    Private Sub btnVersion_Click(sender As System.Object, e As System.EventArgs) Handles btnVersion.Click
        Dim command As String
        command = "Winver"
        Shell(command, 1, True)
        'This button loads the windows version running on the terminal including Service Pack information

    End Sub

    Private Sub btnCertificates_Click(sender As System.Object, e As System.EventArgs) Handles btnCertificates.Click
        Dim Result As DialogResult = MessageBox.Show("Are you sure you want to install the Certificates?", "Install Certificates", MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If Result = DialogResult.Yes Then
            Dim fi As New IO.FileInfo(Application.ExecutablePath)
            Dim Certificatepath As String = IO.Path.Combine(fi.Directory.FullName, "Logmein\Root Certificate update.exe")
            If Not IO.File.Exists(Certificatepath) Then
                MessageBox.Show("Cannot find Certificate Installer Please refer to the Help Guide", "File Path Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                Dim command As String
                command = "C:\Logmein\Root Certificate Update.exe"
                ProcessStart.StartShell(Me, command)
            End If
        ElseIf Result = DialogResult.No Then
            MessageBox.Show("Nothing has been installed", "Installation Status", MessageBoxButtons.OK, MessageBoxIcon.Information
                            )
        End If

        'This will install Certificates for windows xp machines for LogMeIn to work
        'Code Edited 05/11/2015
    End Sub

    Private Sub btnWindows7_Click(sender As System.Object, e As System.EventArgs) Handles btnWindows7.Click
        Dim Result As DialogResult = MessageBox.Show("Are you sure you want to install the Windows 7 and later Version of logmein?", "Install Windows 7+ Version", MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If Result = DialogResult.Yes Then
            Dim fi As New IO.FileInfo(Application.ExecutablePath)
            Dim logmeinpath As String = IO.Path.Combine(fi.Directory.FullName, "\logmein\LogMeIn.msi")

            'Shell("msiexec.exe /i """ & logmeinpath & """ /quiet DEPLOYID=00_rwqt1cykr2hwink6prv2du8rawj0uaxk9cb77 INSTALLMETHOD=5 FQDNDESC=1", 1, True)
            'Return

            If Not IO.File.Exists(logmeinpath) Then
                MessageBox.Show("Cannot find LogMeIn Installer Files Please refer to the Help Guide", "File Path Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                ProcessStart.StartShell(Me, "msiexec.exe", "/i """ & logmeinpath & """ /quiet DEPLOYID=00_f86ljs7205xqat6umnt12fpzef0nmug0rvhmx INSTALLMETHOD=5 FQDNDESC=1", AddressOf UpdateStatus)
            End If
        ElseIf Result = DialogResult.No Then
            MessageBox.Show("Nothing has been installed", "Installation Status", MessageBoxButtons.OK, MessageBoxIcon.Information
                            )
        End If


        'This will install the comtrex version of logmein
        'Code edited 22/08/2016

    End Sub

    Private Sub btnUninstall_Click(sender As System.Object, e As System.EventArgs) Handles btnUninstall.Click

        Dim command As String
        command = "control appwiz.cpl"
        Shell(command, 1, True)

    End Sub

    Private Sub btnWindowsXP_Click(sender As System.Object, e As System.EventArgs) Handles btnWindowsXP.Click
        Dim Result As DialogResult = MessageBox.Show("Are you sure you want to install the Windows XP Version of logmein?", "Install Windows XP Version", MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If Result = DialogResult.Yes Then
            Dim fi As New IO.FileInfo(Application.ExecutablePath)
            Dim logmeinpath As String = IO.Path.Combine(fi.Directory.FullName, "C:\Logmein\Legacy\Legacy.msi")
            If Not IO.File.Exists(logmeinpath) Then
                MessageBox.Show("Cannot find LogMeIn Installer Files Please refer to the Help Guide", "File Path Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                ProcessStart.StartShell(Me, "msiexec.exe", "/i """ & logmeinpath & """ /quiet DEPLOYID=00_rwqt1cykr2hwink6prv2du8rawj0uaxk9cb77 INSTALLMETHOD=5 FQDNDESC=0", AddressOf UpdateStatus)
            End If
        ElseIf Result = DialogResult.No Then
            MessageBox.Show("Nothing has been installed", "Installation Status", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
        'code edit 22/08/2016
        'This will install an older version of logmein for legacy terminals R4 etc do not install on newer terminals
    End Sub

    Private Sub btnCheckLogMeIn_Click(sender As System.Object, e As System.EventArgs) Handles btnCheckLogMeIn.Click
        LogmeinSite.Show()
        MessageBox.Show("If the logmein website does not load you will need to check DNS settings and the Service Pack that is installed.", "LogMeIn Test Connection", MessageBoxButtons.OK, MessageBoxIcon.Information)

        'This loads form 2 linked to URL code on form 2 for testing logmein secure site for connectivity
    End Sub

    Private Sub Button7_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        If (System.IO.Directory.Exists("C:\Logmein")) Then
            System.IO.Directory.Delete("C:\Logmein", True)
        End If
        Application.Exit()
        'code edit 22/08/2016
        'This shutsdown the application via process ID in task manager
    End Sub

    Private Sub Button8_Click(sender As System.Object, e As System.EventArgs) Handles btnConfig.Click
        AboutBox1.Show()
        'MessageBox.Show("LogMeIn Installer Version 4.1.2 - By Peter Brown")
        'This loads the version of the installer in a message box
        'By Peter Brown
    End Sub


    Private Const Service_name As String = "LogMeIn"
    Private Sub UpdateStatus()
        Try
            Dim sc As New ServiceController(Service_name)
            If sc.CanStop Then
                SetStatus(sc.Status.ToString)
            End If
        Catch
            SetStatus("Not Installed")
        End Try
        'code edit 22/08/2016
    End Sub

    Private Sub SetStatus(status As String)
        Me.Invoke(Sub()
                      lblstatus.Text = status
                  End Sub)
        'code edit 22/08/2016
    End Sub

    Private Sub frmmicroconsole_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If Not Me.DesignMode Then
            Dim t As New Threading.Thread(Sub()
                                              UpdateStatus()
                                          End Sub)
            t.Start()
        End If
        'code edit 22/08/2016
    End Sub

    Private Sub Button9_Click(sender As System.Object, e As System.EventArgs) Handles btnHelp.Click
        frmHelp.Show()
        'This opens form3 which is the readme guide and general support
        'refer to here to get information on buld changes
    End Sub
End Class
